# Code Quality Test Project

The `.gitlab-ci.yml` in this project on `master` is set up with Code Quality in [composable AutoDevOps style](https://docs.gitlab.com/ee/topics/autodevops/#using-components-of-auto-devops), but this can be changed if you want to [define the job manually](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Code-Quality.gitlab-ci.yml) to customize it on a new branch.

Documentation:

- [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
- [Code Quality examples](https://docs.gitlab.com/ee/ci/examples/code_quality.html)

## How to use this project

1. Create a new branch.
2. Make a change to `server.rb` that will either increase or decrease overall code quality.
3. Create a merge request.
4. The code quality report will load in the merge request and will be downloadable as an artifact in the job log.